package com.liangyu.service;

import com.liangyu.dto.FileResultDto;

import java.io.IOException;
import java.util.Map;

public interface excelToSqlService {
    Map<String, Object> importExcel(FileResultDto importDto)  throws IOException;
}
