package com.liangyu.service;

import com.liangyu.core.ResultList;
import com.liangyu.dto.FileResultDto;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface FileServerService {
    ResultList fileUpload(MultipartFile[] file, String type);

    List<FileResultDto> getFileInfoList(List<MultipartFile> list);

    String downloadFile(HttpServletRequest request, HttpServletResponse response, String fileUrl);

    //ResultList uploadImg(MultipartFile[] file, String type);
}
