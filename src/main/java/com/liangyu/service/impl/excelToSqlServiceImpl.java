package com.liangyu.service.impl;

import com.liangyu.core.ResultGenerator;
import com.liangyu.dto.FileResultDto;
import com.liangyu.service.excelToSqlService;
import com.liangyu.util.excelUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
public class excelToSqlServiceImpl implements excelToSqlService {

    @Value("${global-config.filePath}")
    private String filepath;

    @Override
    public Map<String, Object> importExcel(FileResultDto importDto)  throws IOException {
        String excelFilePath = filepath + importDto.getFileMaskName();

        Map<String, Object> map = excelUtils.toSql(excelFilePath, importDto.getIsPrimaryKey());
//        if(list == null || list.size() < 0) {
//            throw new RuntimeException("文件数据读取失败");
//        }
        return map;
    }
}
