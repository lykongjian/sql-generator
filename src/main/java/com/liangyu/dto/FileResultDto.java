package com.liangyu.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class FileResultDto implements Serializable {
    //文件原名称
    @ApiModelProperty(value = "文件原名称", example = "测试文件", required = false)
    private String fileName;

    private String filePath;

    /*private String downloadPath;*/
    //文件大小
    @ApiModelProperty(value = "文件大小", example = "1", required = false)
    private Long fileSize;

    private Long proId;

    private Long menuId;

    @JsonFormat(pattern = "yyyy-MM-dd" ,timezone = "GMT+8")
    private Date createTime;

    //private Long typeId;
    //文件唯一编码:盐
    private String fileMask;

    /**
     * 对应菜单的code
     */
    private Long modelCode;

    /**
     * 文件后缀
     */
    @ApiModelProperty(value = "文件名后缀", example = "xls", required = false)
    private String suffix;

    /**
     * 是否将第一行设为主键
     */
    @ApiModelProperty(value = "是否将第一行设为主键(1:是；0:否)", example = "1", required = true)
    private int isPrimaryKey;
    //文件带有'盐'的名称
    private String fileMaskName;
}
