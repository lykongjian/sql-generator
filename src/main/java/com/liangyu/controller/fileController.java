package com.liangyu.controller;

import com.liangyu.core.ResultCode;
import com.liangyu.core.ResultGenerator;
import com.liangyu.core.ResultList;
import com.liangyu.service.FileServerService;
import com.liangyu.util.FileUtil;
import com.liangyu.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * 文件上传
 * @author liangyu
 * @date 2021-12-01 10:30:56
 */
@RestController
@Api(tags = {"文件上传接口"})
@RequestMapping("/file")
public class fileController {

    @Autowired
    private FileServerService fileService;


    @ApiOperation(value = "测试接口", notes = "测试接口")
    @GetMapping("/test")
    public Object test() {
        return ResultGenerator.genSuccess("this is a test controller!");
    }

    @PostMapping(value = "/upload")
    @ApiOperation(value = "文件上传", notes = "文件上传")
    @ResponseBody
    public ResultList fileUpload(@RequestParam(required = false,value = "files") MultipartFile[] files ,
                                 @RequestParam(required = false,value = "type",defaultValue="" ) String type) throws Exception {
        if (!StringUtil.isNullOrEmpty(files) && files.length>0){
            boolean isAllow = true ;
            boolean isEmpty = false ;
            boolean decideName = true ;
            boolean nameLength = false ;
            for (MultipartFile multipartFile:files) {
                if(!FileUtil.isAllowFileType(multipartFile)){
                    isAllow = false ;
                    break;
                }
                if(multipartFile.isEmpty()){
                    isEmpty = true ;
                    break;
                }
                String originalFilename = multipartFile.getOriginalFilename();
                if(StringUtil.decide(originalFilename)){
                    decideName = false;
                    break;
                }
                if(originalFilename.length()>150){
                    nameLength = true;
                    break;
                }
            }
            if(!isAllow){
                ResultList resultList = ResultGenerator.genSuccess(new ArrayList<>());
                resultList.setMessage("上传的文件中有不支持的文件类型，请重新上传");
                resultList.setCode(ResultCode.FILE_UPLOAD_ERROR);
                return resultList ;
            }
            if(!decideName){
                ResultList resultList = ResultGenerator.genSuccess(new ArrayList<>());
                resultList.setMessage("上传的文件名中有特殊字符`$^{}~，请修改文件名后重新上传");
                resultList.setCode(ResultCode.FILE_UPLOAD_ERROR);
                return resultList ;
            }
            if(isEmpty){
                ResultList resultList = ResultGenerator.genSuccess(new ArrayList<>());
                resultList.setMessage("所选文件中存在空文件，请去掉后再上传");
                resultList.setCode(ResultCode.FILE_UPLOAD_ERROR);
                return resultList ;
            }
            if(nameLength){
                ResultList resultList = ResultGenerator.genSuccess(new ArrayList<>());
                resultList.setMessage("上传的文件名中有文件名超出指定长度，请修改文件名后重新上传");
                resultList.setCode(ResultCode.FILE_UPLOAD_ERROR);
                return resultList ;
            }
            return fileService.fileUpload(files,type);
        }
        return ResultGenerator.genSuccess(new ArrayList<>());
    }

    @GetMapping("/download")
    @ApiOperation(value = "文件下载", notes = "文件下载")
    public void downloadFile(HttpServletRequest request, HttpServletResponse response, @RequestParam("fileUrl") String fileUrl) {
        fileService.downloadFile(request,response,fileUrl);
    }


    /**
     *
     * @param files
     * @param type 文件模块（1系统模块,2业务模块）
     * @return
     * @throws Exception
     */
//    @PostMapping(value = "/uploadImg")
//    @ResponseBody
//    public ResultList uploadImg(@RequestParam(required = false,value = "files") MultipartFile[] files ,
//                                @RequestParam(required = false,value = "type",defaultValue="" ) String type) throws Exception {
//        if (!StringUtil.isNullOrEmpty(files) && files.length>0){
//            boolean isAllow = true ;
//            boolean decideName = true ;
//            boolean nameLength = false ;
//            for (MultipartFile multipartFile:files) {
//                if(!FileUtil.isAllowFileType(multipartFile)){
//                    isAllow = false ;
//                    break;
//                }
//                String originalFilename = multipartFile.getOriginalFilename();
//                if(StringUtil.decide(originalFilename)){
//                    decideName = false;
//                    break;
//                }
//                if(originalFilename.length()>150){
//                    nameLength = true;
//                    break;
//                }
//            }
//            if(!decideName){
//                ResultList resultList = ResultGenerator.genSuccess(new ArrayList<>());
//                resultList.setMessage("上传的文件名中有特殊字符，请修改文件名后重新上传");
//                resultList.setCode(ResultCode.FILE_UPLOAD_ERROR);
//                return resultList ;
//            }
//            if(nameLength){
//                ResultList resultList = ResultGenerator.genSuccess(new ArrayList<>());
//                resultList.setMessage("上传的文件名中有文件名超出指定长度，请修改文件名后重新上传");
//                resultList.setCode(ResultCode.FILE_UPLOAD_ERROR);
//                return resultList ;
//            }
//            if(!isAllow){
//                ResultList resultList = ResultGenerator.genSuccess(new ArrayList<>());
//                resultList.setMessage("上传的文件中有不支持的文件类型，请重新上传");
//                resultList.setCode(ResultCode.FILE_UPLOAD_ERROR);
//                return resultList ;
//            }
//            return fileService.uploadImg(files,type);
//        }
//        return ResultGenerator.genSuccess(new ArrayList<>());
//    }
}
