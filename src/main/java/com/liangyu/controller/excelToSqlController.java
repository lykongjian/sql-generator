package com.liangyu.controller;

import com.liangyu.core.Result;
import com.liangyu.core.ResultGenerator;
import com.liangyu.dto.FileResultDto;

import com.liangyu.service.excelToSqlService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;

import java.io.IOException;

/**
 * 服务实现层
 * @author liangyu
 * @date 2021-12-01 10:30:56
 */
@RestController
@Api(tags = {"转化sql接口"})
@RequestMapping("/excel")
public class excelToSqlController {

    @Autowired
    private excelToSqlService sqlService;
    /**
     * excel导入
     * @param importDto
     * @return 更新数据量
     * @throws IOException
     */
    @ApiOperation(value = "excel导入",notes = "excel导入", httpMethod = "POST",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "importDto", value = "文件实体DTO", required = true, dataType = "FileResultDto"),
    })
    @PostMapping("/importExcel")
    public Result importExcel(@RequestBody FileResultDto importDto) throws IOException {
        return ResultGenerator.genSuccessResult(sqlService.importExcel(importDto));
    }
}
