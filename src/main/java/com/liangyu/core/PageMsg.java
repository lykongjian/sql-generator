package com.liangyu.core;

import java.io.Serializable;


public class PageMsg implements Serializable {
	
	/**
     * 页码
     */

    private int pageNumber = 1;

	/**
	 * 每页条数
	 */
	private int pageSize = 10;

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public PageMsg(int pageNumber, int pageSize) {
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
	}

	public PageMsg() {
		super();
	}
     
}
