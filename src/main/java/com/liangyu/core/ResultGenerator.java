package com.liangyu.core;

/**
 * 响应结果生成工具
 */
public class ResultGenerator {
	private static final String DEFAULT_SUCCESS_MESSAGE = "操作成功";

	public static Result genSuccessResult() {
		return new Result().setCode(ResultCode.SUCCESS).setMessage(DEFAULT_SUCCESS_MESSAGE);
	}

	public static Result genSuccessResult(Object rows) {
		return new Result().setCode(ResultCode.SUCCESS).setMessage(DEFAULT_SUCCESS_MESSAGE).setData(rows);
	}

	public static Result genFailResult(String message) {
		return new Result().setCode(ResultCode.FAIL).setMessage(message);
	}

	public static Result genResult(Boolean flag) {
		if(flag){
			return genSuccessResult();
		}else
			return genFailResult("操作失败");
	}

	public static Result genUnauthorizedResult(String message) {
		return new Result().setCode(ResultCode.UNAUTHORIZED).setMessage(message);
	}

	public static ResultList genSuccess(Object rows,long total){
		ResultList resultList = new ResultList();
		ResultList result = resultList.setCode(ResultCode.SUCCESS);
		result.setRows(rows);
		result.setTotal(total);
		return  result;
	}



	public static ResultList genSuccess(Object rows){
		ResultList resultList = new ResultList();
		ResultList result = resultList.setCode(ResultCode.SUCCESS);
		result.setRows(rows);
		return  result;
	}
}
