package com.liangyu.core;


public enum SystemTypeEnum {
    TUNNEL (0,"隧道监测管理系统");

    private Integer code;

    private String menuName;

    private String remark;

    private SystemTypeEnum(Integer code, String menuName) {
        this.code = code;
        this.menuName = menuName;
    }

    private SystemTypeEnum(Integer code, String menuName, String remark) {
        this.code = code;
        this.menuName = menuName;
        this.remark = remark;
    }

    public Integer getCode() {
        return code;
    }

    public String getMenuName() {
        return menuName;
    }

    public String getRemark() {
        return remark;
    }
}
