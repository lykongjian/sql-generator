package com.liangyu.core;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResultList implements Serializable {

    private long total;

    private Object rows;

    private int code;

    private String message ;

    public ResultList setCode(ResultCode resultCode){
        this.code = resultCode.code();
        return this;
    }
}
