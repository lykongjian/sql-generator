package com.liangyu.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 上传的文件
 */
@Data
public class FileResultDto implements Serializable {
    private static final long serialVersionUID = -8772107811849347083L;
    //文件原名称
    @ApiModelProperty(value = "文件原名称", example = "测试文件", required = false)
    private String fileName;
    //文件大小
    @ApiModelProperty(value = "文件大小", example = "1", required = false)
    private Long fileSize;
    //项目id
    @ApiModelProperty(value = "文件所属项目", example = "1", required = false)
    private Long proId;
    //文件后缀名
    @ApiModelProperty(value = "文件名后缀", example = "xls", required = false)
    private String suffix;
    //文件带有'盐'的名称
    private String fileMaskName;
    //上传时间
    @ApiModelProperty(value = "文件名上传时间", example = "xls", required = false)
    @JsonFormat(pattern = "yyyy-MM-dd" ,timezone = "GMT+8")
    private Date createTime;
    //文件唯一编码:盐
    private String fileMask;
}
