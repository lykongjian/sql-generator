package com.liangyu.common.exception;
import java.text.MessageFormat;


public class IaException extends SoftException {

    private static final long serialVersionUID = 1L;

    public IaException(IaExceptionCode code) {
        super(code.get().getErrorCode(), code.get().getMessage());
    }

    public IaException(String msg) {
        super("500", msg);
    }

    public IaException(IaExceptionCode code, Object... format) {
        super(code.get().getErrorCode(), MessageFormat.format(code.get().getMessage(), format));
    }
}