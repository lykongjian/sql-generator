package com.liangyu.common.exception;

import com.liangyu.common.core.IaExceptionMsg;

public enum IaExceptionCode {

    //TODO 500：服务器内部异常
    REDIS_CONNECTION_EXCEPTION(new IaExceptionMsg("500", "服务器系统内部异常")),
    //TODO 401:认证异常
    AUTHENTICATION_EXCEPTION(new IaExceptionMsg("401", "接口，因为认证失败，无法访问系统资源")),
    TOKEN_INVALID(new IaExceptionMsg("401", "无效的token，请重新登录")),
    IS_PROHIBIT(new IaExceptionMsg("401", "用户已经被禁用")),
    //TODO 403：权限异常
    IS_NO_AUTHORITY(new IaExceptionMsg("403", "权限不足，无法访问该资源")),
    //TODO 400:业务异常
    /**
     * 异常测试
     */
    TEST_EXCEPTION(new IaExceptionMsg("80000", "自定义异常测试")),
    //TODO 400:业务异常
    /**
     * 异常测试
     */
    SYNCHRONIZATION_EXCEPTION(new IaExceptionMsg("80000", "自定义异常测试")),
    /**
     * html转pdf异常
     */
    HTML_TO_PDF_EXCEPTION(new IaExceptionMsg("82000", " html转pdf异常")),
    /**
     * 恶意请求不存在主键
     */
    MALICIOUS_REQUESTS(new IaExceptionMsg("81001", "恶意请求不存在主键")),
    /**
     * 不允许存在相同审核人
     */
    NOT_EXAMINE(new IaExceptionMsg("81002", "不允许存在相同审核人")),
    /**
     * 2次密码不一致
     */
    AGAIN_ERROR_PASSWORD(new IaExceptionMsg("81003", "输入的2次密码不一致")),
    IS_ERROR_USER(new IaExceptionMsg("81004", "不存在用户名")),
    IS_ERROR_PASSWORD(new IaExceptionMsg("81005", "密码错误")),
    IS_ERROR_FILE_TYPE(new IaExceptionMsg("81006", "请上传excel、pdf、word、jpg、zip、jpg、png、mp4等格式文件")),
    ACCOUNT_NUMBER_ERROR(new IaExceptionMsg("81007", "错误的文件格式，请上传excel、pdf、word")),
    WORKBOOK_ERROR(new IaExceptionMsg("81008", "Workbook，创建异常")),
    EXCEL_NOT_STANDARD(new IaExceptionMsg("81009", "EXCEL为空或者字段整理不规范")),
    NOT_SYS_URL(new IaExceptionMsg("81010", "传入的sysUrl不是系统提供的目录路径")),
    /**
     * 签章异常
     */
    NO_KEY_WORDS_FOUND_EXCEPTION(new IaExceptionMsg("81010", "电子签章未找到签章关键字")),
    VISA_FAILURE_EXCEPTION(new IaExceptionMsg("81011", "电子签章失败")),

    /**
     * 异常测试
     */
    REPLACE_EXCEPTION(new IaExceptionMsg("81012", "未找到对应的替换值")),

    DATA_NULL_EXCEPTION(new IaExceptionMsg("81013", "数据为空,数据更新失败")),

    DATA_NULL_QUERY_EXCEPTION(new IaExceptionMsg("81015", "数据不存在")),

    SECTION_NOT_BELONG_CURRENT(new IaExceptionMsg("81016", "当前标段非本人所在标段")),
    /**
     * 导入异常
     */
    IMPORT_CELL_NULL_EXCEPTION(new IaExceptionMsg("81014", "导入excel的cell为空"));

    private IaExceptionMsg message;

    private IaExceptionCode(IaExceptionMsg msg) {

        this.message = msg;
    }

    public IaExceptionMsg get() {
        return message;
    }

}
