package com.liangyu.common.core;

import java.io.Serializable;

public class IaExceptionMsg implements Serializable {

    private String errorCode;

    private String message;

    public IaExceptionMsg(String errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
