### SqlGenerator 1.0


目前在线上找到的基本都是根据excel生成新增和更新的sql，其实这些操作可以通过Navicat Premium去实现导入。对我们帮助并不大。

SqlGenerator 1.0 适用通过 **excel** 设计数据库表，经过评审之后再对文档转成数据库表的 **场景** 。

本工具目前是做成接口形式，程序跑起来后直接请求接口上传excel即可生成 **CREATE** 语句

#### excel模板
![输入图片说明](https://images.gitee.com/uploads/images/2022/0104/115338_15aeedbc_7819058.png "屏幕截图.png")

SqlGenerator 后续还会继续更新